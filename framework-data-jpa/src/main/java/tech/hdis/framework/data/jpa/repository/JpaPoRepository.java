package tech.hdis.framework.data.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * 自定义Repository
 *
 * @author 黄志文
 */
@NoRepositoryBean
public interface JpaPoRepository<T> extends JpaRepository<T, String> {
}
