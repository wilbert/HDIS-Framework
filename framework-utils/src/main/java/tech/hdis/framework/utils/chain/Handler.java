package tech.hdis.framework.utils.chain;

/**
 * 抽象责任链模式
 *
 * @author 黄志文
 */
public interface Handler<T, P> {
    /**
     * 得到链表的下一个处理器
     *
     * @return 处理器
     */
    Handler getNextHandler();

    /**
     * 设置链表的下一个处理器
     *
     * @param handler 处理器
     */
    void setNextHandler(Handler handler);

    /**
     * 执行处理器
     *
     * @param params 处理参数
     * @return 处理结果
     */
    T doHandler(P params);
}
