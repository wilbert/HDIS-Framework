package tech.hdis.framework.utils.async;

import lombok.NonNull;

import java.util.concurrent.*;

/**
 * 线程池工厂
 *
 * @author 黄志文
 */
public class ThreadUtils {

    // TODO: 2017/12/4 修改为线程池工厂，进行线程池限流。

    /**
     * 全局线程池
     */
    private static ExecutorService executorService = Executors.newCachedThreadPool();

    /**
     * 执行线程
     *
     * @param command Runnable
     */
    public static void execute(@NonNull Runnable command) {
        executorService.execute(command);
    }

    /**
     * 执行线程
     *
     * @param callable Callable
     * @param <T>      泛型
     * @return Future
     * @throws ExecutionException   错误
     * @throws InterruptedException 错误
     */
    public static <T> Future<T> submit(@NonNull Callable<T> callable) throws ExecutionException, InterruptedException {
        return executorService.submit(callable);
    }
}
