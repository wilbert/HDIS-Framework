package tech.hdis.framework.exception.chain;


import tech.hdis.framework.exception.response.ExceptionResponse;

/**
 * 异常过滤器链表抽象
 *
 * @author 黄志文
 */
public interface ExceptionHandler {

    /**
     * 设置链表的下一个处理器
     *
     * @param handler 处理器
     */
    void setNextHandler(ExceptionHandler handler);

    /**
     * 得到链表的下一个处理器
     *
     * @return 处理器
     */
    ExceptionHandler getNextHandler();

    /**
     * 执行处理器
     *
     * @param excetion 异常
     * @return 异常返回值
     */
    ExceptionResponse doHandler(Exception excetion);
}
