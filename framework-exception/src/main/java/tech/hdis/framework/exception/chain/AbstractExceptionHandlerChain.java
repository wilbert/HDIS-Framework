package tech.hdis.framework.exception.chain;


/**
 * 异步化过滤器链
 *
 * @author 黄志文
 */
public abstract class AbstractExceptionHandlerChain implements ExceptionHandler {

    private ExceptionHandler nextHandler;

    @Override
    public void setNextHandler(ExceptionHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    @Override
    public ExceptionHandler getNextHandler() {
        return this.nextHandler;
    }
}
