package tech.hdis.framework.exception.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tech.hdis.framework.exception.exceptions.FieldError;

import java.io.Serializable;
import java.util.List;

/**
 * 异常返回值
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
public class ExceptionResponse implements Serializable {

    /**
     * 返回码
     */
    private String code;
    /**
     * 附带消息
     */
    private String message;
    /**
     * 验证错误返回值
     */
    private List<FieldError> fieldErrors;

    /**
     * 初始化
     */
    public static ExceptionResponse getInstance() {
        return new ExceptionResponse();
    }

    /**
     * 返回消息组装
     */
    public ExceptionResponse code(String code, String message) {
        this.code = code;
        this.message = message;
        return this;
    }

    /**
     * 验证错误返回值组装
     */
    public ExceptionResponse fieldErrors(List<FieldError> fieldErrors) {
        this.fieldErrors = fieldErrors;
        return this;
    }
}
