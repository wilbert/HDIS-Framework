package tech.hdis.framework.security.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 安全配置
 *
 * @author 黄志文
 */
@Component
@ConfigurationProperties("hdis.session")
@Getter
@Setter
@ToString
public class SecuritySessionProperties {
    /**
     * session过期时间（秒）的key
     */
    public static final String TIMEOUT_KEY = "hdis.session.timeout";

    /**
     * session过期时间（秒）
     */
    public Integer timeout = 604800;
}
