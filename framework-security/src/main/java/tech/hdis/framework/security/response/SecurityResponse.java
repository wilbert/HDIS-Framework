package tech.hdis.framework.security.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 返回值
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
public class SecurityResponse {

    /**
     * 返回码
     */
    private String code;
    /**
     * 附带消息
     */
    private String message;

    /**
     * 返回码组装
     */
    /**
     * 返回码组装
     *
     * @param code    返回码
     * @param message 返回码所代表的消息
     * @return SecurityResponse
     */
    public static SecurityResponse getInstance(String code, String message) {
        SecurityResponse securityResponse = new SecurityResponse();
        securityResponse.setCode(code);
        securityResponse.setMessage(message);
        return securityResponse;
    }
}
