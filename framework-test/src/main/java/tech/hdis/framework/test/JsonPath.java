package tech.hdis.framework.test;

/**
 * 针对RestfulResponse的JsonPath解析模板
 *
 * @author 黄志文
 */
public class JsonPath {

    public static final String CODE = "$.code";
    public static final String MESSAGE = "$.message";
    public static final String DATA = "$.data";
    public static final String PAGE_INFO = "$.data.list[*].";

}
