package tech.hdis.framework.swagger;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;

/**
 * swagger属性配置
 *
 * @author 黄志文
 */
@Configuration
@Validated
@ConfigurationProperties(prefix = "swagger.config")
@Getter
@Setter
@ToString
public class SwaggerProperties {

    @NotBlank(message = "'swagger.config.basePackage' property can not be null.It's the controller package which swagger should be scan.")
    private String basePackage;
    private String title = "API文档";
    private List<String> services = new ArrayList<>();
}
