package tech.hdis.framework.swagger;

import org.springframework.beans.factory.annotation.Autowired;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * SwaggerResource配置文件
 *
 * @author 黄志文
 */
public class SwaggerResourcesProcessor implements SwaggerResourcesProvider {

    private static final String DEFAULT_NAME = "/default";
    private static final String API_DOCS_PATH = "/v2/api-docs";
    private static final String VERSION = "2.0";


    @Autowired
    private SwaggerProperties swaggerProperties;

    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<>();
        resources.add(swaggerResource(DEFAULT_NAME, API_DOCS_PATH, VERSION));
        swaggerProperties.getServices().forEach(service -> resources.add(swaggerResource(service, service + API_DOCS_PATH, VERSION)));
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String url, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setUrl(url);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
