package tech.hdis.framework.oss;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * 统一对象存储服务接口
 *
 * @author 黄志文
 */
public interface OSS {
    /**
     * 获取可用的合法签名
     *
     * @return 阿里云OSS客户端签名
     */
    Map<String, String> getSignature() throws UnsupportedEncodingException;
}
