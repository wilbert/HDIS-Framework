package tech.hdis.framework.data.mongo.manager;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import tech.hdis.framework.data.mongo.po.MongoDataEntity;
import tech.hdis.framework.data.mongo.repository.MongoPoRepository;

/**
 * MongoDB数据库Manager
 *
 * @author 黄志文
 */
public class MongoDataManager<R extends MongoPoRepository<T>, T extends MongoDataEntity> {

    @Autowired
    public R repository;

    public void save(@NonNull Iterable<T> entities) {
        for (T entity : entities) {
            if (StringUtils.isEmpty(entity.getUuid())) {
                entity.onSave();
            } else {
                entity.onUpdate();
            }
        }
        repository.save(entities);
    }

    public void save(@NonNull T entity) {
        if (StringUtils.isEmpty(entity.getUuid())) {
            entity.onSave();
        } else {
            entity.onUpdate();
        }
        repository.save(entity);
    }

}