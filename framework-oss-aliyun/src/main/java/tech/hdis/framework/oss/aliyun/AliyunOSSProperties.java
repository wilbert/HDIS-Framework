package tech.hdis.framework.oss.aliyun;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

/**
 * 阿里云OSS参数配置
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
@Component
@Validated
@ConfigurationProperties("hdis.aliyun.oss")
public class AliyunOSSProperties {
    /**
     * 阿里云OSS的endpoint
     */
    @NotBlank(message = "'hdis.aliyun.oss.endpoint' property can not be null.please find it in your aliyun.")
    public String endpoint = "oss-cn-hangzhou.aliyuncs.com";
    /**
     * 阿里云RAM-AccessKeyId
     */
    @NotBlank(message = "'hdis.aliyun.oss.accessKey' property can not be null.please find it in your aliyun.")
    private String accessKeyId;
    /**
     * 阿里云RAM-AccessKeySecret
     */
    @NotBlank(message = "'hdis.aliyun.oss.accessSecret' property can not be null.please find it in your aliyun.")
    private String accessKeySecret;
    /**
     * 阿里云oss-bucketname
     */
    @NotBlank(message = "'hdis.aliyun.oss.bucket' property can not be null.please find it in your aliyun.")
    private String bucket;
    /**
     * 阿里云oss-dir，指定存储的文件夹，默认根目录
     */
    @NotBlank(message = "'hdis.aliyun.oss.dir' property can not be null.please set it.")
    private String dir = "/";
    /**
     * 阿里云OSS签名过期时间
     */
    @NotNull(message = "'hdis.aliyun.oss.signatureExpireSeconds' property can not be null.please set it.")
    private Integer signatureExpireSeconds;
}
