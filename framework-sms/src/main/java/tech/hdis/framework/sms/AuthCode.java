package tech.hdis.framework.sms;

/**
 * 验证码
 *
 * @author 黄志文
 */
public interface AuthCode {
    /**
     * 发送验证码
     *
     * @param number 电话号码
     * @return 是否发送成功
     * @throws Exception 验证码异常
     */
    boolean sendAuthCode(String number) throws Exception;

    /**
     * 验证验证码
     *
     * @param number   电话号码
     * @param authCode 验证码
     * @return 是否验证成功
     */
    boolean validationAuthCode(String number, String authCode);
}
